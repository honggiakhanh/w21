import logo from './logo.svg';
import './App.css';
import ComponentName from './ComponentName'

function App() {
  return (
    <div className="App">
      <ComponentName/>
    </div>
  );
}

export default App;
